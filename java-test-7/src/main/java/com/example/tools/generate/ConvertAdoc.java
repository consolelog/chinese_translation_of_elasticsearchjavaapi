package com.example.tools.generate;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConvertAdoc {
    public static void convert(List<File> fileList) {
        if (ObjectUtil.isEmpty(fileList)) {
            return;
        }
        for (File file : fileList) {
            String content = FileUtil.readString(file, StandardCharsets.UTF_8);

            String newContent = convert1(content);
            newContent = convert2(newContent);
            newContent = convert3(newContent);
            newContent = convert4(newContent);

            if (!newContent.equals(content)) {
                FileUtil.writeString(newContent, file, StandardCharsets.UTF_8);
            }
        }
    }

    private static String convert4(String content) {
        String regex1 = "--\\s(?=:)([\\s\\S]*?)--";
        Matcher matcher1 = Pattern.compile(regex1, Pattern.MULTILINE).matcher(content);
        if (matcher1.find()) {
            return matcher1.replaceAll("$1");
        } else {
            return content;
        }
    }

    private static String convert3(String content) {
        if (!content.startsWith("\n")) {
            return "\n" + content;
        } else {
            return content;
        }
    }

    private static String convert2(String content) {
        String regex1 = "include-tagged::(.*)\\[(.*)]";
        Matcher matcher1 = Pattern.compile(regex1, Pattern.MULTILINE).matcher(content);
        if (matcher1.find()) {
            return matcher1.replaceAll("include::$1[tag=$2]");
        } else {
            return content;
        }
    }

    private static String convert1(String content) {
        String regex1 = "^(.*)source(.*)subs=(.*)attributes([,|\"])(.*)$";
        Matcher matcher1 = Pattern.compile(regex1, Pattern.MULTILINE).matcher(content);
        if (matcher1.find()) {
            return matcher1.replaceAll("$1source$2subs=$3attributes+$4$5");
        } else {
            return content;
        }
    }
}
