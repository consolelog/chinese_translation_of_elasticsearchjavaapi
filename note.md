# 源码正则替换

```
//((tag|end)*)::(.*)(?<!\[\])$
//$1::$3\[\]

// ((tag|end)*)::(.*)(?<!\[\])$
// $1::$3\[\]

--\s(?=:)([\s\S]*?)--
$1



\A^
\n

\{[a-zA-Z-]+\}

experimental::\[\]
[WARNING]
=====
This functionality is experimental and may be changed or removed completely in a future release. Elastic will take a best effort approach to fix any issues, but experimental features are not subject to the support SLA of official GA features.
=====



\A--\n:api([\s\S]*?)--
\n:api$1

\A(?<!\s)(.*)\n=
\n$1\n=

\A(?!\s)([\s\S]*?)^=
\n$1=


```


asciidoc
```
subs\=\"attributes
subs\=\"attributes+

include(.*)\[(?!tag)(?!leveloffset\=\+1)(.+)\]
include$1[tag=$2]

\[(["]+)source(.+)(?<!indent=0)\]
[$1source$2,indent=0]
```

https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/current/index.html