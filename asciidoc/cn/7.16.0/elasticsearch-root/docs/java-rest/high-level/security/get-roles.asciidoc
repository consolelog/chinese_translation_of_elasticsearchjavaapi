
:api: get-roles
:request: GetRolesRequest
:response: GetRolesResponse

[role="xpack"]
[id="{upid}-{api}"]
=== Get Roles API

[id="{upid}-{api}-request"]
==== Get Roles Request

Retrieving a role can be performed using the `security().getRoles()`
method and by setting the role name on +{request}+:

["source","java",subs="attributes+,callouts,macros",indent=0]
--------------------------------------------------
include::{doc-tests-file}[tag={api}-request]
--------------------------------------------------

Retrieving multiple roles can be performed using the `security().getRoles()`
method and by setting multiple role names on +{request}+:

["source","java",subs="attributes+,callouts,macros",indent=0]
--------------------------------------------------
include::{doc-tests-file}[tag={api}-list-request]
--------------------------------------------------

Retrieving all roles can be performed using the `security().getRoles()`
method without specifying any role names on +{request}+:

["source","java",subs="attributes+,callouts,macros",indent=0]
--------------------------------------------------
include::{doc-tests-file}[tag={api}-all-request]
--------------------------------------------------

include::../execution.asciidoc[]

[id="{upid}-{api}-response"]
==== Get Roles Response

The returned +{response}+ allows getting information about the retrieved roles as follows.

["source","java",subs="attributes+,callouts,macros",indent=0]
--------------------------------------------------
include::{doc-tests-file}[tag={api}-response]
--------------------------------------------------